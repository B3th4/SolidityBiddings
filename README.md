SolidityBids
------------------
-------------------

This is a small project I made after having followed the course https://www.udemy.com/ethereum-and-solidity-the-complete-developers-guide on udemy.

I did this to familiarize myself with Solidity and Javascript (more precisely react, next and semantic-ui).

The idea was to use smartcontracts to create biddings: decentralize the bidding process so that no 'arbitrator' is needed, everything is handeled by contracts.

This project is an exercice and certainly not the most optimal way to achieve what I did.

There are 4 types of contracts: Biddingcreator, user, item, bidding.

-Biddingcreator: this is the contract that is used to create the 3 other types of contract and to interact with those.

-User: this contract defines who you are on the blockchain by allowing you to describe yourself.

-Item: defines the items that are possesed/currently in biddings. Each item contract specifies by who it is owned and has a history of its value and previous owners.

-Bidding: this contract is used to trade the items and to bid on those. One Bidding contract is used per bidding, as well as one Item per bidding.


How to install and run the project:
----------------------------------------

In chrome browser install: https://metamask.io/

linux:
------

install nodeJS: 

in terminal: sudo apt-get install npm

install ganache, web3 and mocha: 

in terminal: npm install --save mocha ganache-cli web3@1.0.0-beta.26

install solidity compiler: 

in terminal: npm install --save solc


Mac OS:
-----

install brew: 

in terminal: ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

install nodeJS: 

in terminal: brew install node

in SolidityBiddings remove node_modules(those are the modules for linux as I worked with linux)

install npm:

in terminal: npm install 

install ganache, web3 and mocha: 

in terminal: npm install --save mocha ganache-cli web3@1.0.0-beta.26

install solidity compiler: 

in terminal: npm install --save solc




Commands(run in terminal @ ./Ethereum Folder):
---------------------------------------

node compile.js --> compile the Solidity contract

node deploy.js --> deploy the contract to the Rinskby testnet (this returns an address after some time. Keep it, it is the address of the mastercontract!)

npm run test --> run the testscript to see if core funcionalities are working

npm run dev --> start the server for the Web app ( to be accessed in chrome/mozilla with metamask at localhost:3000)


You can change the address of the mastercontract in ./Ethereum/BiddingCreator.js


