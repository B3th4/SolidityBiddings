import React, {Component} from 'react';
import biddingCreator from '../../Ethereum/BiddingCreator';
import Bidding from '../../Ethereum/Bidding';
import Item from '../../Ethereum/Item';
import {Card, Button,Grid, Form, Input, Message, Container,List} from 'semantic-ui-react';
import web3 from '../../Ethereum/web3';
import Layout from '../../components/Layout'
import { Link,Router } from '../../routes';


class ShowBidding extends Component{

  state={
    value:0,
    errorMessage:'',
    loading:false,
  };

  static async getInitialProps(props){
    const biddingAddress= props.query.address;
    const bid = await Bidding(biddingAddress);
    var summaryBid = await bid.methods.summary().call();
    var formatedSummaryBid={itemAddress:summaryBid[0], creator:summaryBid[1], highestBidder:summaryBid[2], minimumBid:summaryBid[3], value:summaryBid[4], biddingAddress:biddingAddress, finished:summaryBid[5]}
    if(formatedSummaryBid.finished===false){
      formatedSummaryBid.finished= "Ongoing"
    }
    else(formatedSummaryBid.finished="Bidding finished")

    var itemAddress =summaryBid[0];
    const item = await Item(itemAddress);
    var summaryItem=await item.methods.summaryItem().call();
    var formatedSummaryItem={name:summaryItem[0], description: summaryItem[1], itemAddress: itemAddress}

    const biddings = await biddingCreator.methods.getBiddings().call();

    var accounts = await web3.eth.getAccounts();

    var account = accounts[0];

    const infoUser = await biddingCreator.methods.getInfoUser(account).call();

    const exists = infoUser[1];

    return {exists,formatedSummaryBid, formatedSummaryItem, bid, account };
  }


  onSubmitBidMore = async (event) =>{
    event.preventDefault();

    this.setState({loading:true, errorMessage:''});
    try{
      const accounts = await web3.eth.getAccounts();


      await this.props.bid.methods
      .bid().send({
        from: accounts[0],
        value: web3.utils.toWei(this.state.value,'ether')
      });

      Router.pushRoute('/bid/'+this.props.formatedSummaryBid.biddingAddress);
    } catch(err){
      this.setState({errorMessage:err.message});
    }

    this.setState({loading:false});
  };

  onSubmitFinalize = async (event) =>{
    event.preventDefault();

    var d = new Date();
    var t = d.getTime();

    this.setState({loading:true, errorMessage:''});
    try{
      const accounts = await web3.eth.getAccounts();


      await biddingCreator.methods
      .finalizeBidding(this.props.formatedSummaryBid.biddingAddress,t).send({
        from: accounts[0],
      });

      Router.pushRoute('/bid/'+this.props.formatedSummaryBid.biddingAddress);
    } catch(err){
      this.setState({errorMessage:err.message});
    }

    this.setState({loading:false});
  };




renderNoAccount(){
  return(
  <Container>

      <Link route="/user/new">
        <a>
          <Button
            floated="right"
            content="Create Account"
            icon="add circle"
            primary
            fluide="true"
          />
        </a>
      </Link>

        No active Metamask account found. Several reasons may cause this issue.

        <List bulleted>
          <List.Item>You do not have an account yet to communicate with this blockchain. Please create one</List.Item>
          <List.Item>You do not have metamask installed or are logged out. Please install Metamask</List.Item>
          <List.Item>There is a bug. Please let me know</List.Item>
        </List>



    </Container>);

}

renderNotCreator(){
  const ButtonNotCreator = () => (
    <Form onSubmit={this.onSubmitBidMore} error={!!this.state.errorMessage}>
    <Form.Field>
    <label>Enter the amount you want to bid: </label>
    <Input
    label="ETH"
    labelPosition="right"
    value={this.state.value}
    onChange={event => this.setState({value:event.target.value})}
    />
    </Form.Field>
    <Message error header="Oops!" content={this.state.errorMessage} />
    <Button loading={this.state.loading} primary>Bid!</Button>
    </Form>
  )

  return ButtonNotCreator();
}

renderCreator(){
  const ButtonCreator = () => (
      <Form onSubmit={this.onSubmitFinalize} error={!!this.state.errorMessage}>
      <Message error header="Oops!" content={this.state.errorMessage} />
      <Button loading={this.state.loading} primary>Finalize the Bidding</Button>
      </Form>
    )
return ButtonCreator();
}

renderButton(){
    if(this.props.formatedSummaryBid.finished == "Bidding finished"){

    return ("This bidding is over. The item has been sold.")
    }

    else if (!this.props.exists) {

      return this.renderNoAccount();
    }

    else if(this.props.account == this.props.formatedSummaryBid.creator){
      return this.renderCreator();
    }

    else{
      return this.renderNotCreator();
    }

  }


renderItem(){
  var item =this.props.formatedSummaryItem

  const card = () => (
    <Card fluid>
      <Card.Content>
        <Card.Header>{item.name}</Card.Header>
        <Card.Meta>
        <Link route={`/item/${item.itemAddress}`}>
          <a>More info: {item.itemAddress}</a>
        </Link>
        </Card.Meta>
        <Card.Description>{item.description}</Card.Description>
      </Card.Content>
    </Card>
  )

  return card();
  }



  renderBidding(){
    var bidding =this.props.formatedSummaryBid

    const card = () => (
      <Card fluid>
        <Card.Content>
          <Card.Header>Bidding contract: {bidding.biddingAddress}</Card.Header>

          <Card.Description>
          Creator of the contract: {bidding.creator}<br></br>
          Starting value of bidding: {web3.utils.fromWei(bidding.minimumBid, 'ether')} ETH<br></br>
          Current value of bidding: {web3.utils.fromWei(bidding.value, 'ether')} ETH<br></br>
          Current highest bidder: {bidding.highestBidder}<br></br>
          BiddingStatus: {bidding.finished}</Card.Description>
        </Card.Content>
      </Card>
    )
    return card();
  }



  render(){

    return(
      <Layout>
       <div>

       <Grid columns={2} relaxed>
        <Grid.Column width={10}>
          <h3> Show Bid</h3>
          <font size="4">Item of the Bidding:</font>
           {this.renderItem()}
           <font size="4">Summary of the Bidding:</font>
           {this.renderBidding()}
       </Grid.Column>
       <Grid.Column width={6} verticalAlign='middle'>
        {this.renderButton()}
      </Grid.Column>
       </Grid>
    </div>
    </Layout>
  )
  }
}

export default ShowBidding;
