import React, {Component} from 'react';
import biddingCreator from '../../Ethereum/BiddingCreator';
import Item from '../../Ethereum/Item';
import {Card, Button, Form, Input, Message, Dropdown} from 'semantic-ui-react';
import web3 from '../../Ethereum/web3';
import Layout from '../../components/Layout'
import { Link,Router } from '../../routes';


class NewBidding extends Component{
  state={
    itemName:'',
    minimalBid:0,
    errorMessage:'',
    value: [],
    loading:false,
  };
  static async getInitialProps(){
    var [account] = await web3.eth.getAccounts();
    const infoUser = await biddingCreator.methods.getInfoUser(account).call();

    const exists = infoUser[1];
    const itemsAdresses = infoUser[2];
    var items;
    if (exists){
      items =await Promise.all( itemsAdresses.map(async (address,index) => {

        var doesUserHasItem = await biddingCreator.methods.doesUserHasItem(account,address).call();
        const item = Item(address);
        var summary = await item.methods.summaryItem().call();
        return{
          name: summary[0],
          address: address,
          hasItem: doesUserHasItem
          }
    }));
  }

    return{exists, items};
}

onSubmit= async (event) =>{
  event.preventDefault();
  var d = new Date();
  var t = d.getTime();

  this.setState({loading:true, errorMessage:''});
  try{
  const accounts = await web3.eth.getAccounts();
  await biddingCreator.methods
  .createBidding(this.state.value,accounts[0],web3.utils.toWei(this.state.minimalBid, 'ether'),t).send({
    from: accounts[0],
  });

  Router.pushRoute('/mybiddings');
} catch(err){
  this.setState({errorMessage:err.message});
}

this.setState({loading:false});
};

handleChange = (e, { value }) =>{
  this.setState({ value })
}
renderItemList(){
  var _items=this.props.items;
  var itemList = _items.map( (item) =>{
    if(item.hasItem==1){
      return{
        key: item.address,
        text: item.name +" ("+item.address+")",
        value: item.address
      };
    }

  });
  return itemList.filter(element => element !== undefined);
}


// <option key={index} value= onChange={event => {
//   this.setState({itemName:event.target.value});
// }
// }
//
//   >{item.name} ({item.address})</option>

  render(){

    var formList=this.renderItemList();

    return(
      <Layout>
       <div>
       <h3>Create a bidding:</h3>
       <Form onSubmit={this.onSubmit} error={!!this.state.errorMessage}>
          Choose the Item you want to create a bidding for:
         <Dropdown
           options={formList}
           value={this.state.value}
           placeholder='Choose Item...'
           onChange={this.handleChange}
           selection
           fluid
         />


         <Form.Field>
         <label>Bidding start amount</label>
         <Input
         label="ETH"
         labelPosition="right"
         value={this.state.minimalBid}
         onChange={event => this.setState({minimalBid:event.target.value})}
         />
        </Form.Field>
        <Message error header="Oops!" content={this.state.errorMessage} />
        <Button loading={this.state.loading} primary>Create Bidding</Button>
   </Form>
    </div>
    </Layout>
  )
  }
}

export default NewBidding;
