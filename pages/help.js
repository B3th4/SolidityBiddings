import React, {Component} from 'react';
import biddingCreator from '../Ethereum/BiddingCreator';
import User from '../Ethereum/User';
import {Card, Button, Container, Grid, Segment, Divider, Input,List, Form,Message} from 'semantic-ui-react';
import web3 from '../Ethereum/web3';
import Layout from '../components/Layout'
import { Link,Router } from '../routes';


class Help extends Component{

    render(){

      return(
        <Layout>
        <div>

        <h3>Help</h3>
        <p>To use this Website it is required to have Metamask installed the browser.<br />Metamask can be downloaded here:&nbsp;<a href="https://metamask.io/">https://metamask.io/</a></p>
<ol>
<li>The 'Ethereum Bidding' Pages shows the Bidding that have been created on the Ethereum Blockchain using the mastercontract.</li>
<li>'My Profile' shows a customized description of yourself. This will be visible to &nbsp;other users. Your description can be changed on this page.</li>
<li>'My Items' can be used to see the Items you have added to the chain aswell as to add new Items.</li>
<li>'My Biddings' is used to see the biddings you have created and to create new ones</li>
</ol>
<p>The Usual scenario for a Bidding on the platfrom is the following one:</p>
<ul>
<li>A user (creator) creates a bidding for one of his items and specifies the starting amount in ETH. By creating the offer the creator 'gives' the item to the mastercontract. The mastercontract will handle the bidding.</li>
<li>Other users can than bid on this bidding/contract. The money they Bid is given to the mastercontract. If someone bids more they get there money back.</li>
<li>The creator ends the bidding. He receives the money from the mastercotnract and the highest bidder receives the item of the bidding.</li>
</ul>
<p>This project is an exercice and should not be used for 'real life' biddings.&nbsp;</p>
<p>More info and source code:&nbsp;<a href="https://gitlab.com/B3th4/SolidityBiddings">https://gitlab.com/B3th4/SolidityBiddings</a></p>
        </div>
        </Layout>
      )

  }
}
  export default Help;
