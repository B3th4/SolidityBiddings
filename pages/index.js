import React, {Component} from 'react';
import biddingCreator from '../Ethereum/BiddingCreator';
import {Card, Button} from 'semantic-ui-react';
import web3 from '../Ethereum/web3';
import Layout from '../components/Layout'
import { Link } from '../routes';


class BiddingCreator extends Component{

  static async getInitialProps(){
    const biddings = await biddingCreator.methods.getBiddings().call();

    const items =await Promise.all( biddings.map(async address => {

      var summary = await biddingCreator.methods.viewBidding(address).call();

      var _header = summary[0]
      if(summary[2]==true){
        _header=summary[0]+ " (Bidding finished)"
      }
      return {
        header: _header,
        description:"Current Highest Bid: "+ web3.utils.fromWei(summary[1], 'ether') + "ETH",
        extra:"Bidding status: "+ summary[2],
        meta:"Bidding contract address: "+address
      };
    }));

    return { biddings, items};
  }


 renderBiddings(){
   const items = this.props.biddings.map((address,i) => {
     return {
       header: this.props.items[i].header,
       description: this.props.items[i].description,
       meta:(
         <Link route={`/bid/${address}`}>
           <a>{this.props.items[i].meta}</a>
         </Link>
       ),
       fluid: true
     };
   });

    return <Card.Group items={items} />;
}


  render(){

    return(
      <Layout>
       <div>

       <h3>Biddings</h3>

       <Link route="/bid/new">
         <a>
           <Button
             floated="right"
             content="Create new Bidding"
             icon="add circle"
             primary
           />
         </a>
       </Link>

    {this.renderBiddings()}

    </div>
    </Layout>
  )
  }
}

export default BiddingCreator;
