import React, {Component} from 'react';
import biddingCreator from '../Ethereum/BiddingCreator';
import User from '../Ethereum/User';
import Item from '../Ethereum/Item';
import {Card, Button, Container, Grid, Segment, Divider, Input,List} from 'semantic-ui-react';
import web3 from '../Ethereum/web3';
import Layout from '../components/Layout'
import { Link } from '../routes';


class MyBiddings extends Component{


  static async getInitialProps(){
    var [account] = await web3.eth.getAccounts();
    const infoUser = await biddingCreator.methods.getInfoUser(account).call();

    const exists = infoUser[1];
    const itemsAdresses = infoUser[2];
    var items;
    var biddingsCards;
    if (exists){
      items =await Promise.all( itemsAdresses.map(async (address) => {

        const item = Item(address);
        var summary = await item.methods.summaryItem().call();
        var doesUserHasItem = await biddingCreator.methods.doesUserHasItem(account,address).call();

        return{
          address: address,
          name: summary[0],
          hasItem:doesUserHasItem
        };

    }));
  var biddings =await Promise.all( items.map(async (item) =>{
    if(item.hasItem ==2){
      var bid = await biddingCreator.methods.getBiddingForItem(item.address).call();
      return {biddingAddress: bid, name: item.name};
    }
  }));
biddings= biddings.filter(element => element !== undefined);


biddingsCards =await Promise.all( biddings.map(async (bid) => {

  var summary = await biddingCreator.methods.viewBidding(bid.biddingAddress).call();
   return {
     header: bid.name,
     description:"Current Highest Bid: "+ web3.utils.fromWei(summary[1], 'ether') + " ETH",
     meta: "Bidding contract address: "+bid.biddingAddress,
     address:bid.biddingAddress
   };

}));
}
return{exists, biddingsCards};
}

renderMyBiddings(){

  const items = this.props.biddingsCards.map((bid) => {
    return {
      header: bid.header,
      description: bid.description,
      meta:(
        <Link route={`/bid/${bid.address}`}>
          <a>{bid.meta}</a>
        </Link>
      ),
      fluid: true
    };
  });

   return(
     <Container>
     <Link route="/bid/new">
       <a>
         <Button
           floated="right"
           content="Create new Bidding"
           icon="add circle"
           primary
         />
       </a>
     </Link>
     <Card.Group items={items} />
     </Container>);
}

renderNoUser(){
  return( <Container>

    <Link route="/user/new">
      <a>
        <Button
          floated="right"
          content="Create Account"
          icon="add circle"
          primary
        />
      </a>
    </Link>

      No active Metamask account found. Several reasons may cause this issue.
      <List bulleted>
        <List.Item>You do not have an account yet to communicate with this blockchain. Please create one</List.Item>
        <List.Item>You do not have metamask installed or are logged out. Please install Metamask</List.Item>
        <List.Item>There is a bug. Please let me know</List.Item>
      </List>



  </Container>);
}

chooseRender(){
  if(this.props.exists){
    return this.renderMyBiddings();
  }
  else{
    return this.renderNoUser();
  }
}

  render(){

    return(
      <Layout>
       <div>
       <h3>My Items in Biddings</h3>
       {this.chooseRender()}
    </div>

    </Layout>
  )
  }
}

export default MyBiddings;
