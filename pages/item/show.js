import React, {Component} from 'react';
import Items from '../../Ethereum/Item';
import biddingCreator from '../../Ethereum/BiddingCreator';
import {Card, Button, List, Item,Form, Input, Label, Grid} from 'semantic-ui-react';
import web3 from '../../Ethereum/web3';
import Layout from '../../components/Layout'
import DescriptionItem from '../../components/DescriptionItem'
import { Link } from '../../routes';


class ItemShow extends Component{
  state={
    description:'',
    errorMessage:'',
    loading:false,
  };

  static async getInitialProps(props){
    const itemAddress= props.query.address;
    const item = await Items(itemAddress);
    const summary = await item.methods.summaryItem().call();
    const itemName= summary[0];
    const description= summary[1];
    const publicOwnerAddress= summary[2];
    const historyLength= summary[3];

    const ownerAddress = await biddingCreator.methods.getUser(publicOwnerAddress).call();

    var history =[];
    for(var index=0; index<historyLength; index++ ){
      var historyItem = await item.methods.getHistory(index).call();
      history.push(historyItem);
    }
    return {itemName,description,ownerAddress,history,itemAddress};
  }


  renderItemBasic(){
    var address=this.props.ownerAddress
    const items = [
      {
        header: this.props.itemName,
        meta:
        <Link route={`/user/${address}`}>
          <a>address of owner: {address}</a>
        </Link>,

        style: { overflowWrap: 'break-word' }
      },

      {
        header: "Description",
        description:
          this.props.description,
        style: { overflowWrap: 'break-word' }
      },

    ];

    return <Card.Group items={items} />;

  }

  renderHistory(){
    var htmlHistory = this.props.history.map((hist,index) => {
        var ownerAddress=hist[0];
        var buyPrice=hist[1];
        var date = new Date(parseInt(hist[2]));
        date=date.toString();

        return(
      <List.Item key={index}>
      <List.Content>
        <List.Header>{date}</List.Header>
        <List.Description>Public key of owner at that time: {ownerAddress}</List.Description>
        <List.Description>Buy Price: {web3.utils.fromWei(buyPrice, 'ether')} ETH</List.Description>
      </List.Content>
    </List.Item>
  );});
  return(
    <Item>
    <Item.Header><b>Item History:</b></Item.Header>
    <List divided relaxed>
    {htmlHistory}
     </List>
    </Item>
  );
  }

  render(){

    return(
      <Layout>
       <div>

       <h3>{this.props.itemName}: {this.props.itemAddress}</h3>
       <Grid>
       <Grid.Column width={8}>
       {this.renderItemBasic()}
       </Grid.Column>
       <Grid.Column width={8}>
       <DescriptionItem address={this.props.itemAddress}/>
       </Grid.Column>
       </Grid>


       <hr></hr>
       {this.renderHistory()}

    </div>
    </Layout>
  )
  }
}

export default ItemShow;
