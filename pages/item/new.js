import React, { Component } from 'react';
import { Form, Button, Input, Message,Container,List } from 'semantic-ui-react';;
import web3 from '../../Ethereum/web3';
import Layout from '../../components/Layout'
import { Link,Router } from '../../routes';
import biddingCreator from '../../Ethereum/BiddingCreator';


class newItem extends Component{
  state={
    itemName:'',
    description:'',
    value:0,
    errorMessage:'',
    loading:false,
  };

  static async getInitialProps(){
    var [account] = await web3.eth.getAccounts();
    const infoUser = await biddingCreator.methods.getInfoUser(account).call();
    const exists = infoUser[1];
    return{exists};
}


  onSubmit= async (event) =>{
    event.preventDefault();

    var d = new Date();
    var t = d.getTime();

    this.setState({loading:true, errorMessage:''});
    try{
    const accounts = await web3.eth.getAccounts();
    await biddingCreator.methods
    .createItem(this.state.itemName,this.state.description,web3.utils.toWei(this.state.value, 'ether'),t).send({
      from: accounts[0],
    });

    Router.pushRoute('/myitems');
  } catch(err){
    this.setState({errorMessage:err.message});
  }

  this.setState({loading:false});
};


renderItems(){
  return(
    <Form onSubmit={this.onSubmit} error={!!this.state.errorMessage}>
    <Form.Field>
     <label>Item Name</label>
     <Input
     value={this.state.itemName}
     onChange={event => this.setState({itemName:event.target.value})}
     />
    </Form.Field>
    <Form.Field>
     <label>Description</label>
     <Input
     value={this.state.description}
     onChange={event => this.setState({description:event.target.value})}
     />
    </Form.Field>
    <Form.Field>
     <label>Value</label>
     <Input
     label="ETH"
     labelPosition="right"
     value={this.state.value}
     onChange={event => this.setState({value:event.target.value})}
     />
    </Form.Field>
    <Message error header="Oops!" content={this.state.errorMessage} />
    <Button loading={this.state.loading} primary>Create Item</Button>
    </Form>

  );
}

renderNoUser(){
  return( <Container>
      No active Metamask account found. Several reasons may cause this issue.
      <List bulleted>
        <List.Item>You do not have an account yet to communicate with this blockchain. Please create one</List.Item>
        <List.Item>You do not have metamask installed or are logged out. Please install Metamask</List.Item>
        <List.Item>There is a bug. Please let me know</List.Item>
      </List>

    <Link route="/newUser">
      <a>
        <Button
          floated="right"
          content="Create Account"
          icon="add circle"
          primary
        />
      </a>
    </Link>

  </Container>);
}

chooseRender(){
  if(this.props.exists){
    return this.renderItems();
  }
  else{
    return this.renderNoUser();
  }
}

  render(){

    return(
      <Layout>
       <div>

       <h3>Add a new Item</h3>
       {this.chooseRender()}
    </div>
    </Layout>
  )
  }
}

export default newItem;
