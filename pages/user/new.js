import React, { Component } from 'react';
import { Form, Button, Input, Message,Container,List } from 'semantic-ui-react';;
import web3 from '../../Ethereum/web3';
import Layout from '../../components/Layout'
import { Link,Router } from '../../routes';
import biddingCreator from '../../Ethereum/BiddingCreator';


class addUser extends Component{

  static async getInitialProps(){
    var [account] = await web3.eth.getAccounts();
    const infoUser = await biddingCreator.methods.getInfoUser(account).call();
    const exists = infoUser[1];
    return{exists};
}

  state={
    description:'',
    errorMessage:'',
    loading:false,
  };

  onSubmit= async (event) =>{
    event.preventDefault();
    this.setState({loading:true, errorMessage:''});
    try{
    const accounts = await web3.eth.getAccounts();
    await biddingCreator.methods
    .createUser(this.state.description).send({
      from: accounts[0],
    });
    Router.pushRoute('/myprofile');
  } catch(err){
    this.setState({errorMessage:err.message});
  }
  this.setState({loading:false});
};


renderNewUserForm(){
  return(
    <Form onSubmit={this.onSubmit} error={!!this.state.errorMessage}>
    <Form.Field>
     <label>User Description</label>
     <Input
     value={this.state.description}
     onChange={event => this.setState({description:event.target.value})}
     />
    </Form.Field>
    <Message error header="Oops!" content={this.state.errorMessage} />
    <Button loading={this.state.loading} primary>Create User</Button>
    </Form>

  );
}

renderAlreadyUser(){
  return(
      <a>There is already an account for this address!</a>);
}

chooseRender(){
  if(this.props.exists){
    return this.renderAlreadyUser();
  }
  else{
    return this.renderNewUserForm();
  }
}

  render(){

    return(
      <Layout>
       <div>

       <h3>Create new User </h3>
       {this.chooseRender()}
    </div>
    </Layout>
  )
  }
}

export default addUser;
