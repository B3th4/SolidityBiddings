import React, {Component} from 'react';
import User from '../../Ethereum/User';
import {Card, Button} from 'semantic-ui-react';
import web3 from '../../Ethereum/web3';
import Layout from '../../components/Layout'
import { Link } from '../../routes';


class BiddingCreator extends Component{
  static async getInitialProps(props){
    const userAddress= props.query.address;
    const user = await User(userAddress);
    const description = await user.methods.getDescription().call();
    return {userAddress,description};
  }


  render(){
    return(
    <Layout>
     <div>

     <h3>User</h3>
  <Card
    fluid
    header={this.props.userAddress}
    description={this.props.description}
  />

  </div>
  </Layout>
);
  }
}

export default BiddingCreator;
