import React, {Component} from 'react';
import biddingCreator from '../Ethereum/BiddingCreator';
import User from '../Ethereum/User';
import {Card, Button, Container, Grid, Segment, Divider, Input,List, Form,Message} from 'semantic-ui-react';
import web3 from '../Ethereum/web3';
import Layout from '../components/Layout'
import { Link,Router } from '../routes';


class MyProfile extends Component{
  state={
    description:'',
    errorMessage:'',
    loading:false,
  };

  static async getInitialProps(){
    var [account] = await web3.eth.getAccounts();
    const infoUser = await biddingCreator.methods.getInfoUser(account).call();

    const userAddress=infoUser[0];
    const exists = infoUser[1];
    var userDescription;
    var user;
    if (exists){
      user = User(userAddress);
      userDescription =await user.methods.description().call();
    }
    return{exists, userDescription,user,userAddress};
  }


  onSubmit= async (event) =>{
    event.preventDefault();

    this.setState({loading:true, errorMessage:''});
    try{
      const accounts = await web3.eth.getAccounts();
      await this.props.user.methods
      .changeDescription(this.state.description).send({
        from: accounts[0],
      });

      Router.pushRoute('/myprofile');
    } catch(err){
      this.setState({errorMessage:err.message});
    }

    this.setState({loading:false});
  };

  renderUser(){

    return(

      <Grid columns={2} relaxed>
      <Grid.Column>
      <Segment basic>
      <Card fluid>

      <Card.Content>
      <Card.Header>User Description</Card.Header>
      <Card.Description>{this.props.userDescription}</Card.Description>
      <Card.Meta>{this.props.userAddress}</Card.Meta>
      </Card.Content>

      </Card>
      </Segment>
      </Grid.Column>
      <Grid.Column>
      <Form onSubmit={this.onSubmit} error={!!this.state.errorMessage}>
      <Form.Field>
      <label>Change Description</label>
      <Input
      value={this.state.description}
      onChange={event => this.setState({description:event.target.value})}
      />
      </Form.Field>
      <Message error header="Oops!" content={this.state.errorMessage} />
      <Button loading={this.state.loading} primary>Change Description</Button>
      </Form>
      </Grid.Column>
      </Grid>


    );

  }

  renderNoUser(){
    return( <Container>

      <Link route="/user/new">
      <a>
      <Button
      floated="right"
      content="Create Account"
      icon="add circle"
      primary
      />
      </a>
      </Link>

      No active Metamask account found. Several reasons may cause this issue.
      <List bulleted>
      <List.Item>You do not have an account yet to communicate with this blockchain. Please create one</List.Item>
      <List.Item>You do not have metamask installed or are logged out. Please install Metamask</List.Item>
      <List.Item>There is a bug. Please let me know</List.Item>
      </List>




      </Container>);
    }

    chooseRender(){
      if(this.props.exists){
        return this.renderUser();
      }
      else{
        return this.renderNoUser();
      }
    }

    render(){

      return(
        <Layout>
        <div>

        <h3>My Profile</h3>
        {this.chooseRender()}
        </div>
        </Layout>
      )
    }
  }

  export default MyProfile;
