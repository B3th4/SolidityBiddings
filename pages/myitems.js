import React, {Component} from 'react';
import biddingCreator from '../Ethereum/BiddingCreator';
import User from '../Ethereum/User';
import Item from '../Ethereum/Item';
import {Card, Button, Container, Grid, Segment, Divider, Input,List} from 'semantic-ui-react';
import web3 from '../Ethereum/web3';
import Layout from '../components/Layout'
import { Link } from '../routes';


class MyItems extends Component{


  static async getInitialProps(){
    var [account] = await web3.eth.getAccounts();
    const infoUser = await biddingCreator.methods.getInfoUser(account).call();

    const exists = infoUser[1];
    const itemsAdresses = infoUser[2];
    var items;
    if (exists){
      items =await Promise.all( itemsAdresses.map(async (address,index) => {

        const item = Item(address);
        var summary = await item.methods.summaryItem().call();
        var doesUserHasItem = await biddingCreator.methods.doesUserHasItem(account,address).call();

        return{

          key: address,
          header: summary[0],
          description:"Description: "+ summary[1],
          meta:<Link route={`/item/${address}`}>
            <a>{address}</a>
          </Link>,
          hasItem:doesUserHasItem
        };

    }));
  }
    return{exists, items};
}

renderCardGroup(){
  var _items=this.props.items;
var itemList = _items.map( (item) =>{
  if(item.hasItem==1){
    return{
      key: item.key,
      header: item.header,
      description:item.description,
      meta:item.meta,
      fluid:true
    };
  }
});
return itemList.filter(element => element !== undefined);
}

renderItems(){

   return(
     <div>
     <Link route="/item/new">
       <a>
         <Button
           floated="right"
           content="Add new Item"
           icon="add circle"
           primary
         />
       </a>
     </Link>

     <Card.Group items={this.renderCardGroup()} />
     </div>);
}

renderNoUser(){
  return( <Container>

    <Link route="/user/new">
      <a>
        <Button
          floated="right"
          content="Create Account"
          icon="add circle"
          primary
        />
      </a>
    </Link>

      No active Metamask account found. Several reasons may cause this issue.
      <List bulleted>
        <List.Item>You do not have an account yet to communicate with this blockchain. Please create one</List.Item>
        <List.Item>You do not have metamask installed or are logged out. Please install Metamask</List.Item>
        <List.Item>There is a bug. Please let me know</List.Item>
      </List>



  </Container>);
}

chooseRender(){
  if(this.props.exists){
    return this.renderItems();
  }
  else{
    return this.renderNoUser();
  }
}

  render(){

    return(
      <Layout>
       <div>
       <h3>My Items</h3>
       {this.chooseRender()}
    </div>

    </Layout>
  )
  }
}

export default MyItems;
