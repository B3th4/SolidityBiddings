const routes = require('next-routes')();

routes
  .add('/user/new', '/user/new')
  .add('/user/:address', '/user/show')
  .add('/bid/new', '/bid/new')
  .add('/bid/:address', '/bid/show')
  .add('/item/new', '/item/new')
  .add('/item/:address', '/item/show');

module.exports = routes;
