const assert = require('assert');
const ganache = require("ganache-cli");
const Web3 = require('web3');
const web3 = new Web3(ganache.provider());

const compiledBiddingCreator = require('../Ethereum/build/BiddingCreator.json');
const compiledBidding = require('../Ethereum/build/Bidding.json');
const compiledItem = require('../Ethereum/build/Item.json');
const compiledUser = require('../Ethereum/build/User.json')

let accounts;
let biddingCreator;
let user1;
let userAddress1;
let user2;
let userAddress2;
let user3;
let userAddress3;
let item;
let itemAddress;
let Bidding;
let BiddingAddress;


require('events').EventEmitter.defaultMaxListeners = 0;

beforeEach( async () => {
  accounts = await web3.eth.getAccounts();

  biddingCreator = await new web3.eth.Contract(JSON.parse(compiledBiddingCreator.interface))
  .deploy({data: compiledBiddingCreator.bytecode})
  .send({from: accounts[3], gas: '4500000'});

  await biddingCreator.methods.createUser("This is the first user").send({
    from: accounts[0],
    gas: '1000000'
  });

  userAddress1 = await biddingCreator.methods.getUser(accounts[0]).call();
  user1 = await new web3.eth.Contract(
    JSON.parse(compiledUser.interface),
    userAddress1
  );

  await biddingCreator.methods.createUser("This is the second user").send({
    from: accounts[1],
    gas: '1000000'
  });

  userAddress2 = await biddingCreator.methods.getUser(accounts[1]).call();
  user2 = await new web3.eth.Contract(
    JSON.parse(compiledUser.interface),
    userAddress2
  );

  await biddingCreator.methods.createUser("This is the third user").send({
    from: accounts[2],
    gas: '1000000'
  });

  userAddress3 = await biddingCreator.methods.getUser(accounts[2]).call();
  user3 = await new web3.eth.Contract(
    JSON.parse(compiledUser.interface),
    userAddress3
  );

  await biddingCreator.methods.createItem("Desktop Screen","This is my new Pc Screen",5,1000).send({
    from: accounts[0],
    gas: '1000000'
  });

  let items = await biddingCreator.methods.getItemsFromUser(accounts[0]).call();

  itemAddress=items[0];
  item = await new web3.eth.Contract(
    JSON.parse(compiledItem.interface),
    itemAddress
  );


});





describe('Deploy the contract', async () => {
  it('deploys the masterContract', () => {
    assert.ok(biddingCreator.options.address)
  });
});


describe('Users', () => {
  it('create user 1/3', async () => {

    assert.ok(user1.options.address)
  });

  it('create user 2/3', async () => {

    assert.ok(user2.options.address)
  });
  it('create user 3/3', async () => {

    assert.ok(user2.options.address)
  });
  it('User1 can modify his description', async ()=> {
    await user1.methods.changeDescription('New description for user1').send({
      from: accounts[0],
      gas:'1000000'
    });

    const newDescription = await user1.methods.description().call();

    assert.ok(newDescription == 'New description for user1');

  });

  it('User2 can not change description of User1', async () => {
    try {
      await user1.methods.changeDescription('New description for user1').send({
        from: accounts[1],
        gas:'1000000'
      });
    } catch (err) {
      assert(err);
    }

  });

});

describe('Items', async () => {


  it('User1 creates an item', async () => {

    assert.ok(item.options.address);
  });

  it('User1 owns Item 1/2', async ()=> {
    let hasItem = await biddingCreator.methods.doesUserHasItem(accounts[0], itemAddress).call();
    assert.ok(hasItem == 1);

  });

  it('User1 owns Item 2/2', async ()=> {
    let owner = await item.methods.owner().call();
    assert.ok(owner==accounts[0])
  });

  it('User2 does not own the Item', async ()=> {
    let hasItem = await biddingCreator.methods.doesUserHasItem(accounts[1], itemAddress).call();
    assert.ok(hasItem != 1);
  });

  it('User1 can modify Item description (owner)', async ()=> {
    owner = await item.methods.owner().call();

    try {
      await item.methods.changeDescription('New description for Item1').send({
        from: accounts[0],
        gas:'1000000'
      });
    } catch (err) {
      console.log(err);
    }
    newDescription= await item.methods.description().call();
    assert.ok(newDescription == 'New description for Item1');

  });

  it('User2 can not change description of Item (not owner)', async () => {
    try {
      await item.methods.changeDescription('New description for Item1').send({
        from: accounts[1],
        gas:'1000000'
      });
    } catch (err) {
      assert(err);
    }

  });

  it('User1(owner) can give his Item to User2 1/2', async () => {
    await biddingCreator.methods.giveItem(itemAddress, userAddress2, 0, 1200).send({
      from: accounts[0],
      gas: '1000000'
    });
    let newOwner = await item.methods.owner().call();
    assert.ok(newOwner == userAddress2);
  });

  it('User1(owner) can give his Item to User2 2/2', async () => {
    await biddingCreator.methods.giveItem(itemAddress, userAddress2, 0, 1200).send({
      from: accounts[0],
      gas: '1000000'
    });

    let hasUser1item= await biddingCreator.methods.doesUserHasItem(accounts[0], itemAddress).call();
    let hasUser2item= await biddingCreator.methods.doesUserHasItem(accounts[1], itemAddress).call();

    assert.ok( hasUser1item == 2 && hasUser2item == 1);
  });

  it('User2 (not owner) can not give Item to user3', async ()=>{
    try {
      await biddingCreator.methods.giveItem(itemAddress, userAddress3, 0, 1200).send({
        from: accounts[1],
        gas: '1000000'
      });
      assert(false);
    } catch (err) {
      assert(err);
    }

  });

});

describe('Biddings', async () => {
  it('User1 can create a Biddingoffer for Item', async () => {
    await biddingCreator.methods.createBidding(itemAddress,accounts[0],100,1300).send({
      from: accounts[0],
      gas: '1000000'
    });

    let addressBid= await biddingCreator.methods.biddings(0).call();

    bid = await new web3.eth.Contract(
      JSON.parse(compiledBidding.interface),
      addressBid
    );
    assert.ok(bid.options.address);
  });

  it('User1 can not create a second Biddingoffer for same Item', async () => {
    await biddingCreator.methods.createBidding(itemAddress,accounts[0],100,1300).send({
      from: accounts[0],
      gas: '1000000'
    });

    try {
      await biddingCreator.methods.createBidding(itemAddress,accounts[0],100,1300).send({
        from: accounts[0],
        gas: '1000000'
      });
      assert(false);
    } catch (err) {
      assert(err);
    }
  });

  it('User2 can make a bid on the offer', async ()=> {
    await biddingCreator.methods.createBidding(itemAddress,accounts[0],100,1300).send({
      from: accounts[0],
      gas: '1000000'
    });

    let addressBid= await biddingCreator.methods.biddings(0).call();

    bid = await new web3.eth.Contract(
      JSON.parse(compiledBidding.interface),
      addressBid
    );
    await bid.methods.bid().send({
      value: '200',
      from: accounts[1]
    });

    highestBidder=await bid.methods.highestBidder().call();
    currentBid=await bid.methods.currentBid().call();
    assert.ok(currentBid > 100 && highestBidder == accounts[1])

  });

  it('User2 can not make a bid offer if he offers less than minimumBid', async ()=> {
    await biddingCreator.methods.createBidding(itemAddress,accounts[0],100,1300).send({
      from: accounts[0],
      gas: '1000000'
    });

    let addressBid= await biddingCreator.methods.biddings(0).call();

    bid = await new web3.eth.Contract(
      JSON.parse(compiledBidding.interface),
      addressBid
    );

    try {
      await bid.methods.bid().send({
        value: '2',
        from: accounts[1]
      });
      assert(false);
    } catch (err) {
      assert(err);
    }
  });

  it('User1 can close the Bidding', async ()=> {
    await biddingCreator.methods.createBidding(itemAddress,accounts[0],100,1300).send({
      from: accounts[0],
      gas: '1000000'
    });

    let addressBid= await biddingCreator.methods.biddings(0).call();

    bid = await new web3.eth.Contract(
      JSON.parse(compiledBidding.interface),
      addressBid
    );

    await bid.methods.endBidding().send({
      from: accounts[0],
      gas: '1000000'
    });
    finished = await bid.methods.finished().call();
    assert.ok(finished==true);

  });

  it('User2 cant bid on closed Bidding', async ()=> {
    await biddingCreator.methods.createBidding(itemAddress,accounts[0],100,1300).send({
      from: accounts[0],
      gas: '1000000'
    });

    let addressBid= await biddingCreator.methods.biddings(0).call();

    bid = await new web3.eth.Contract(
      JSON.parse(compiledBidding.interface),
      addressBid
    );

    await bid.methods.endBidding().send({
      from: accounts[0],
      gas: '1000000'
    });
    finished = await bid.methods.finished().call();
    try {
      await bid.methods.bid().send({
        value: '200',
        from: accounts[1]
      });
      assert(false);
    } catch (err) {
      assert(err);
    }
  });




  it('finalizeBidding by user 1, user2 gets item', async () => {
    await biddingCreator.methods.createBidding(itemAddress,accounts[0],100,1300).send({
      from: accounts[0],
      gas: '1000000'
    });

    let addressBid= await biddingCreator.methods.biddings(0).call();

    bid = await new web3.eth.Contract(
      JSON.parse(compiledBidding.interface),
      addressBid
    );

    await bid.methods.bid().send({
      value: '200',
      from: accounts[1]
    });


    await biddingCreator.methods.finalizeBidding(addressBid,500).send({
      from: accounts[0],
      gas: '1000000'
    });

    let newOwner = await item.methods.owner().call();
    assert.ok(newOwner==accounts[1])

    let hasItem = await biddingCreator.methods.doesUserHasItem(accounts[1], itemAddress).call();
    assert.ok(hasItem == 1);

  });

});
