'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _promise = require('babel-runtime/core-js/promise');

var _promise2 = _interopRequireDefault(_promise);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _BiddingCreator = require('../Ethereum/BiddingCreator');

var _BiddingCreator2 = _interopRequireDefault(_BiddingCreator);

var _semanticUiReact = require('semantic-ui-react');

var _web = require('../Ethereum/web3');

var _web2 = _interopRequireDefault(_web);

var _Layout = require('../components/Layout');

var _Layout2 = _interopRequireDefault(_Layout);

var _routes = require('../routes');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _jsxFileName = '/home/florent/Desktop/SolidityEthereum/Bidding/pages/index.js?entry';


var BiddingCreator = function (_Component) {
  (0, _inherits3.default)(BiddingCreator, _Component);

  function BiddingCreator() {
    (0, _classCallCheck3.default)(this, BiddingCreator);

    return (0, _possibleConstructorReturn3.default)(this, (BiddingCreator.__proto__ || (0, _getPrototypeOf2.default)(BiddingCreator)).apply(this, arguments));
  }

  (0, _createClass3.default)(BiddingCreator, [{
    key: 'renderBiddings',
    value: function renderBiddings() {
      var _this2 = this;

      var items = this.props.biddings.map(function (address, i) {
        console.log(i);
        console.log(address);
        return {
          header: _this2.props.items[i].header,
          description: _this2.props.items[i].description,
          meta: _react2.default.createElement(_routes.Link, { route: '/bid/' + address, __source: {
              fileName: _jsxFileName,
              lineNumber: 37
            }
          }, _react2.default.createElement('a', {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 38
            }
          }, _this2.props.items[i].meta)),
          fluid: true
        };
      });

      return _react2.default.createElement(_semanticUiReact.Card.Group, { items: items, __source: {
          fileName: _jsxFileName,
          lineNumber: 45
        }
      });
    }
  }, {
    key: 'render',
    value: function render() {

      return _react2.default.createElement(_Layout2.default, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 52
        }
      }, _react2.default.createElement('div', {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 53
        }
      }, _react2.default.createElement('h3', {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 55
        }
      }, 'Biddings'), _react2.default.createElement(_routes.Link, { route: '/bid/new', __source: {
          fileName: _jsxFileName,
          lineNumber: 57
        }
      }, _react2.default.createElement('a', {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 58
        }
      }, _react2.default.createElement(_semanticUiReact.Button, {
        floated: 'right',
        content: 'Create new Bidding',
        icon: 'add circle',
        primary: true,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 59
        }
      }))), this.renderBiddings()));
    }
  }], [{
    key: 'getInitialProps',
    value: function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2() {
        var _this3 = this;

        var biddings, items;
        return _regenerator2.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return _BiddingCreator2.default.methods.getBiddings().call();

              case 2:
                biddings = _context2.sent;
                _context2.next = 5;
                return _promise2.default.all(biddings.map(function () {
                  var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(address) {
                    var summary;
                    return _regenerator2.default.wrap(function _callee$(_context) {
                      while (1) {
                        switch (_context.prev = _context.next) {
                          case 0:
                            _context.next = 2;
                            return _BiddingCreator2.default.methods.viewBidding(address).call();

                          case 2:
                            summary = _context.sent;
                            return _context.abrupt('return', {
                              header: summary[0],
                              description: "Current Highest Bid: " + _web2.default.utils.fromWei(summary[1], 'ether') + " ETH",
                              meta: address
                            });

                          case 4:
                          case 'end':
                            return _context.stop();
                        }
                      }
                    }, _callee, _this3);
                  }));

                  return function (_x) {
                    return _ref2.apply(this, arguments);
                  };
                }()));

              case 5:
                items = _context2.sent;
                return _context2.abrupt('return', { biddings: biddings, items: items });

              case 7:
              case 'end':
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function getInitialProps() {
        return _ref.apply(this, arguments);
      }

      return getInitialProps;
    }()
  }]);

  return BiddingCreator;
}(_react.Component);

exports.default = BiddingCreator;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInVua25vd24iXSwibmFtZXMiOlsiX2pzeEZpbGVOYW1lIiwiQmlkZGluZ0NyZWF0b3IiLCJfQ29tcG9uZW50IiwiX19wcm90b19fIiwiYXBwbHkiLCJhcmd1bWVudHMiLCJrZXkiLCJ2YWx1ZSIsInJlbmRlckJpZGRpbmdzIiwiX3RoaXMyIiwiaXRlbXMiLCJwcm9wcyIsImJpZGRpbmdzIiwibWFwIiwiYWRkcmVzcyIsImkiLCJjb25zb2xlIiwibG9nIiwiaGVhZGVyIiwiZGVzY3JpcHRpb24iLCJtZXRhIiwiY3JlYXRlRWxlbWVudCIsInJvdXRlIiwiX19zb3VyY2UiLCJmaWxlTmFtZSIsImxpbmVOdW1iZXIiLCJmbHVpZCIsIkdyb3VwIiwicmVuZGVyIiwiZmxvYXRlZCIsImNvbnRlbnQiLCJpY29uIiwicHJpbWFyeSIsIl9yZWYiLCJtYXJrIiwiX2NhbGxlZTIiLCJfdGhpczMiLCJ3cmFwIiwiX2NhbGxlZTIkIiwiX2NvbnRleHQyIiwicHJldiIsIm5leHQiLCJtZXRob2RzIiwiZ2V0QmlkZGluZ3MiLCJjYWxsIiwic2VudCIsImFsbCIsIl9yZWYyIiwiX2NhbGxlZSIsInN1bW1hcnkiLCJfY2FsbGVlJCIsIl9jb250ZXh0Iiwidmlld0JpZGRpbmciLCJhYnJ1cHQiLCJ1dGlscyIsImZyb21XZWkiLCJzdG9wIiwiX3giLCJnZXRJbml0aWFsUHJvcHMiXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFFQTs7OztBQUNBOzs7O0FBQ0E7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBTkEsSUFBSUEsZUFBZSxxRUFBbkI7OztBQVFBLElBQUlDLGlCQUFpQixVQUFVQyxVQUFWLEVBQXNCO0FBQ3pDLDBCQUFVRCxjQUFWLEVBQTBCQyxVQUExQjs7QUFFQSxXQUFTRCxjQUFULEdBQTBCO0FBQ3hCLGtDQUFnQixJQUFoQixFQUFzQkEsY0FBdEI7O0FBRUEsV0FBTyx5Q0FBMkIsSUFBM0IsRUFBaUMsQ0FBQ0EsZUFBZUUsU0FBZixJQUE0Qiw4QkFBdUJGLGNBQXZCLENBQTdCLEVBQXFFRyxLQUFyRSxDQUEyRSxJQUEzRSxFQUFpRkMsU0FBakYsQ0FBakMsQ0FBUDtBQUNEOztBQUVELDZCQUFhSixjQUFiLEVBQTZCLENBQUM7QUFDNUJLLFNBQUssZ0JBRHVCO0FBRTVCQyxXQUFPLFNBQVNDLGNBQVQsR0FBMEI7QUFDL0IsVUFBSUMsU0FBUyxJQUFiOztBQUVBLFVBQUlDLFFBQVEsS0FBS0MsS0FBTCxDQUFXQyxRQUFYLENBQW9CQyxHQUFwQixDQUF3QixVQUFVQyxPQUFWLEVBQW1CQyxDQUFuQixFQUFzQjtBQUN4REMsZ0JBQVFDLEdBQVIsQ0FBWUYsQ0FBWjtBQUNBQyxnQkFBUUMsR0FBUixDQUFZSCxPQUFaO0FBQ0EsZUFBTztBQUNMSSxrQkFBUVQsT0FBT0UsS0FBUCxDQUFhRCxLQUFiLENBQW1CSyxDQUFuQixFQUFzQkcsTUFEekI7QUFFTEMsdUJBQWFWLE9BQU9FLEtBQVAsQ0FBYUQsS0FBYixDQUFtQkssQ0FBbkIsRUFBc0JJLFdBRjlCO0FBR0xDLGdCQUFNLGdCQUFNQyxhQUFOLGVBRUosRUFBRUMsT0FBTyxVQUFVUixPQUFuQixFQUE0QlMsVUFBVTtBQUNsQ0Msd0JBQVV4QixZQUR3QjtBQUVsQ3lCLDBCQUFZO0FBRnNCO0FBQXRDLFdBRkksRUFPSixnQkFBTUosYUFBTixDQUNFLEdBREYsRUFFRTtBQUNFRSxzQkFBVTtBQUNSQyx3QkFBVXhCLFlBREY7QUFFUnlCLDBCQUFZO0FBRko7QUFEWixXQUZGLEVBUUVoQixPQUFPRSxLQUFQLENBQWFELEtBQWIsQ0FBbUJLLENBQW5CLEVBQXNCSyxJQVJ4QixDQVBJLENBSEQ7QUFxQkxNLGlCQUFPO0FBckJGLFNBQVA7QUF1QkQsT0ExQlcsQ0FBWjs7QUE0QkEsYUFBTyxnQkFBTUwsYUFBTixDQUFvQixzQkFBS00sS0FBekIsRUFBZ0MsRUFBRWpCLE9BQU9BLEtBQVQsRUFBZ0JhLFVBQVU7QUFDN0RDLG9CQUFVeEIsWUFEbUQ7QUFFN0R5QixzQkFBWTtBQUZpRDtBQUExQixPQUFoQyxDQUFQO0FBS0Q7QUF0QzJCLEdBQUQsRUF1QzFCO0FBQ0RuQixTQUFLLFFBREo7QUFFREMsV0FBTyxTQUFTcUIsTUFBVCxHQUFrQjs7QUFFdkIsYUFBTyxnQkFBTVAsYUFBTixtQkFFTDtBQUNFRSxrQkFBVTtBQUNSQyxvQkFBVXhCLFlBREY7QUFFUnlCLHNCQUFZO0FBRko7QUFEWixPQUZLLEVBUUwsZ0JBQU1KLGFBQU4sQ0FDRSxLQURGLEVBRUU7QUFDRUUsa0JBQVU7QUFDUkMsb0JBQVV4QixZQURGO0FBRVJ5QixzQkFBWTtBQUZKO0FBRFosT0FGRixFQVFFLGdCQUFNSixhQUFOLENBQ0UsSUFERixFQUVFO0FBQ0VFLGtCQUFVO0FBQ1JDLG9CQUFVeEIsWUFERjtBQUVSeUIsc0JBQVk7QUFGSjtBQURaLE9BRkYsRUFRRSxVQVJGLENBUkYsRUFrQkUsZ0JBQU1KLGFBQU4sZUFFRSxFQUFFQyxPQUFPLFVBQVQsRUFBcUJDLFVBQVU7QUFDM0JDLG9CQUFVeEIsWUFEaUI7QUFFM0J5QixzQkFBWTtBQUZlO0FBQS9CLE9BRkYsRUFPRSxnQkFBTUosYUFBTixDQUNFLEdBREYsRUFFRTtBQUNFRSxrQkFBVTtBQUNSQyxvQkFBVXhCLFlBREY7QUFFUnlCLHNCQUFZO0FBRko7QUFEWixPQUZGLEVBUUUsZ0JBQU1KLGFBQU4sMEJBQTRCO0FBQzFCUSxpQkFBUyxPQURpQjtBQUUxQkMsaUJBQVMsb0JBRmlCO0FBRzFCQyxjQUFNLFlBSG9CO0FBSTFCQyxpQkFBUyxJQUppQjtBQUsxQlQsa0JBQVU7QUFDUkMsb0JBQVV4QixZQURGO0FBRVJ5QixzQkFBWTtBQUZKO0FBTGdCLE9BQTVCLENBUkYsQ0FQRixDQWxCRixFQTZDRSxLQUFLakIsY0FBTCxFQTdDRixDQVJLLENBQVA7QUF3REQ7QUE1REEsR0F2QzBCLENBQTdCLEVBb0dJLENBQUM7QUFDSEYsU0FBSyxpQkFERjtBQUVIQyxXQUFPLFlBQVk7QUFDakIsVUFBSTBCLE9BQU8saUNBQW1CLGFBQWEsc0JBQW9CQyxJQUFwQixDQUF5QixTQUFTQyxRQUFULEdBQW9CO0FBQ3RGLFlBQUlDLFNBQVMsSUFBYjs7QUFFQSxZQUFJeEIsUUFBSixFQUFjRixLQUFkO0FBQ0EsZUFBTyxzQkFBb0IyQixJQUFwQixDQUF5QixTQUFTQyxTQUFULENBQW1CQyxTQUFuQixFQUE4QjtBQUM1RCxpQkFBTyxDQUFQLEVBQVU7QUFDUixvQkFBUUEsVUFBVUMsSUFBVixHQUFpQkQsVUFBVUUsSUFBbkM7QUFDRSxtQkFBSyxDQUFMO0FBQ0VGLDBCQUFVRSxJQUFWLEdBQWlCLENBQWpCO0FBQ0EsdUJBQU8seUJBQWVDLE9BQWYsQ0FBdUJDLFdBQXZCLEdBQXFDQyxJQUFyQyxFQUFQOztBQUVGLG1CQUFLLENBQUw7QUFDRWhDLDJCQUFXMkIsVUFBVU0sSUFBckI7QUFDQU4sMEJBQVVFLElBQVYsR0FBaUIsQ0FBakI7QUFDQSx1QkFBTyxrQkFBU0ssR0FBVCxDQUFhbEMsU0FBU0MsR0FBVCxDQUFhLFlBQVk7QUFDM0Msc0JBQUlrQyxRQUFRLGlDQUFtQixhQUFhLHNCQUFvQmIsSUFBcEIsQ0FBeUIsU0FBU2MsT0FBVCxDQUFpQmxDLE9BQWpCLEVBQTBCO0FBQzdGLHdCQUFJbUMsT0FBSjtBQUNBLDJCQUFPLHNCQUFvQlosSUFBcEIsQ0FBeUIsU0FBU2EsUUFBVCxDQUFrQkMsUUFBbEIsRUFBNEI7QUFDMUQsNkJBQU8sQ0FBUCxFQUFVO0FBQ1IsZ0NBQVFBLFNBQVNYLElBQVQsR0FBZ0JXLFNBQVNWLElBQWpDO0FBQ0UsK0JBQUssQ0FBTDtBQUNFVSxxQ0FBU1YsSUFBVCxHQUFnQixDQUFoQjtBQUNBLG1DQUFPLHlCQUFlQyxPQUFmLENBQXVCVSxXQUF2QixDQUFtQ3RDLE9BQW5DLEVBQTRDOEIsSUFBNUMsRUFBUDs7QUFFRiwrQkFBSyxDQUFMO0FBQ0VLLHNDQUFVRSxTQUFTTixJQUFuQjtBQUNBLG1DQUFPTSxTQUFTRSxNQUFULENBQWdCLFFBQWhCLEVBQTBCO0FBQy9CbkMsc0NBQVErQixRQUFRLENBQVIsQ0FEdUI7QUFFL0I5QiwyQ0FBYSwwQkFBMEIsY0FBS21DLEtBQUwsQ0FBV0MsT0FBWCxDQUFtQk4sUUFBUSxDQUFSLENBQW5CLEVBQStCLE9BQS9CLENBQTFCLEdBQW9FLE1BRmxEO0FBRy9CN0Isb0NBQU1OO0FBSHlCLDZCQUExQixDQUFQOztBQU1GLCtCQUFLLENBQUw7QUFDQSwrQkFBSyxLQUFMO0FBQ0UsbUNBQU9xQyxTQUFTSyxJQUFULEVBQVA7QUFmSjtBQWlCRDtBQUNGLHFCQXBCTSxFQW9CSlIsT0FwQkksRUFvQktaLE1BcEJMLENBQVA7QUFxQkQsbUJBdkIyQyxDQUFoQyxDQUFaOztBQXlCQSx5QkFBTyxVQUFVcUIsRUFBVixFQUFjO0FBQ25CLDJCQUFPVixNQUFNM0MsS0FBTixDQUFZLElBQVosRUFBa0JDLFNBQWxCLENBQVA7QUFDRCxtQkFGRDtBQUdELGlCQTdCZ0MsRUFBYixDQUFiLENBQVA7O0FBK0JGLG1CQUFLLENBQUw7QUFDRUssd0JBQVE2QixVQUFVTSxJQUFsQjtBQUNBLHVCQUFPTixVQUFVYyxNQUFWLENBQWlCLFFBQWpCLEVBQTJCLEVBQUV6QyxVQUFVQSxRQUFaLEVBQXNCRixPQUFPQSxLQUE3QixFQUEzQixDQUFQOztBQUVGLG1CQUFLLENBQUw7QUFDQSxtQkFBSyxLQUFMO0FBQ0UsdUJBQU82QixVQUFVaUIsSUFBVixFQUFQO0FBN0NKO0FBK0NEO0FBQ0YsU0FsRE0sRUFrREpyQixRQWxESSxFQWtETSxJQWxETixDQUFQO0FBbURELE9BdkQwQyxDQUFoQyxDQUFYOztBQXlEQSxlQUFTdUIsZUFBVCxHQUEyQjtBQUN6QixlQUFPekIsS0FBSzdCLEtBQUwsQ0FBVyxJQUFYLEVBQWlCQyxTQUFqQixDQUFQO0FBQ0Q7O0FBRUQsYUFBT3FELGVBQVA7QUFDRCxLQS9ETTtBQUZKLEdBQUQsQ0FwR0o7O0FBd0tBLFNBQU96RCxjQUFQO0FBQ0QsQ0FsTG9CLGtCQUFyQjs7a0JBb0xlQSxjIiwiZmlsZSI6InVua25vd24iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgX3JlZ2VuZXJhdG9yUnVudGltZSBmcm9tICdiYWJlbC1ydW50aW1lL3JlZ2VuZXJhdG9yJztcbmltcG9ydCBfUHJvbWlzZSBmcm9tICdiYWJlbC1ydW50aW1lL2NvcmUtanMvcHJvbWlzZSc7XG5pbXBvcnQgX2FzeW5jVG9HZW5lcmF0b3IgZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL2FzeW5jVG9HZW5lcmF0b3InO1xuaW1wb3J0IF9PYmplY3QkZ2V0UHJvdG90eXBlT2YgZnJvbSAnYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9nZXQtcHJvdG90eXBlLW9mJztcbmltcG9ydCBfY2xhc3NDYWxsQ2hlY2sgZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJztcbmltcG9ydCBfY3JlYXRlQ2xhc3MgZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzJztcbmltcG9ydCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybiBmcm9tICdiYWJlbC1ydW50aW1lL2hlbHBlcnMvcG9zc2libGVDb25zdHJ1Y3RvclJldHVybic7XG5pbXBvcnQgX2luaGVyaXRzIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9pbmhlcml0cyc7XG52YXIgX2pzeEZpbGVOYW1lID0gJy9ob21lL2Zsb3JlbnQvRGVza3RvcC9Tb2xpZGl0eUV0aGVyZXVtL0JpZGRpbmcvcGFnZXMvaW5kZXguanM/ZW50cnknO1xuaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCBiaWRkaW5nQ3JlYXRvciBmcm9tICcuLi9FdGhlcmV1bS9CaWRkaW5nQ3JlYXRvcic7XG5pbXBvcnQgeyBDYXJkLCBCdXR0b24gfSBmcm9tICdzZW1hbnRpYy11aS1yZWFjdCc7XG5pbXBvcnQgd2ViMyBmcm9tICcuLi9FdGhlcmV1bS93ZWIzJztcbmltcG9ydCBMYXlvdXQgZnJvbSAnLi4vY29tcG9uZW50cy9MYXlvdXQnO1xuaW1wb3J0IHsgTGluayB9IGZyb20gJy4uL3JvdXRlcyc7XG5cbnZhciBCaWRkaW5nQ3JlYXRvciA9IGZ1bmN0aW9uIChfQ29tcG9uZW50KSB7XG4gIF9pbmhlcml0cyhCaWRkaW5nQ3JlYXRvciwgX0NvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gQmlkZGluZ0NyZWF0b3IoKSB7XG4gICAgX2NsYXNzQ2FsbENoZWNrKHRoaXMsIEJpZGRpbmdDcmVhdG9yKTtcblxuICAgIHJldHVybiBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybih0aGlzLCAoQmlkZGluZ0NyZWF0b3IuX19wcm90b19fIHx8IF9PYmplY3QkZ2V0UHJvdG90eXBlT2YoQmlkZGluZ0NyZWF0b3IpKS5hcHBseSh0aGlzLCBhcmd1bWVudHMpKTtcbiAgfVxuXG4gIF9jcmVhdGVDbGFzcyhCaWRkaW5nQ3JlYXRvciwgW3tcbiAgICBrZXk6ICdyZW5kZXJCaWRkaW5ncycsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlckJpZGRpbmdzKCkge1xuICAgICAgdmFyIF90aGlzMiA9IHRoaXM7XG5cbiAgICAgIHZhciBpdGVtcyA9IHRoaXMucHJvcHMuYmlkZGluZ3MubWFwKGZ1bmN0aW9uIChhZGRyZXNzLCBpKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKGkpO1xuICAgICAgICBjb25zb2xlLmxvZyhhZGRyZXNzKTtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICBoZWFkZXI6IF90aGlzMi5wcm9wcy5pdGVtc1tpXS5oZWFkZXIsXG4gICAgICAgICAgZGVzY3JpcHRpb246IF90aGlzMi5wcm9wcy5pdGVtc1tpXS5kZXNjcmlwdGlvbixcbiAgICAgICAgICBtZXRhOiBSZWFjdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgICAgTGluayxcbiAgICAgICAgICAgIHsgcm91dGU6ICcvYmlkLycgKyBhZGRyZXNzLCBfX3NvdXJjZToge1xuICAgICAgICAgICAgICAgIGZpbGVOYW1lOiBfanN4RmlsZU5hbWUsXG4gICAgICAgICAgICAgICAgbGluZU51bWJlcjogMzdcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgICAgICdhJyxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIF9fc291cmNlOiB7XG4gICAgICAgICAgICAgICAgICBmaWxlTmFtZTogX2pzeEZpbGVOYW1lLFxuICAgICAgICAgICAgICAgICAgbGluZU51bWJlcjogMzhcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIF90aGlzMi5wcm9wcy5pdGVtc1tpXS5tZXRhXG4gICAgICAgICAgICApXG4gICAgICAgICAgKSxcbiAgICAgICAgICBmbHVpZDogdHJ1ZVxuICAgICAgICB9O1xuICAgICAgfSk7XG5cbiAgICAgIHJldHVybiBSZWFjdC5jcmVhdGVFbGVtZW50KENhcmQuR3JvdXAsIHsgaXRlbXM6IGl0ZW1zLCBfX3NvdXJjZToge1xuICAgICAgICAgIGZpbGVOYW1lOiBfanN4RmlsZU5hbWUsXG4gICAgICAgICAgbGluZU51bWJlcjogNDVcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAncmVuZGVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuXG4gICAgICByZXR1cm4gUmVhY3QuY3JlYXRlRWxlbWVudChcbiAgICAgICAgTGF5b3V0LFxuICAgICAgICB7XG4gICAgICAgICAgX19zb3VyY2U6IHtcbiAgICAgICAgICAgIGZpbGVOYW1lOiBfanN4RmlsZU5hbWUsXG4gICAgICAgICAgICBsaW5lTnVtYmVyOiA1MlxuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcbiAgICAgICAgICAnZGl2JyxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBfX3NvdXJjZToge1xuICAgICAgICAgICAgICBmaWxlTmFtZTogX2pzeEZpbGVOYW1lLFxuICAgICAgICAgICAgICBsaW5lTnVtYmVyOiA1M1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0sXG4gICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcbiAgICAgICAgICAgICdoMycsXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIF9fc291cmNlOiB7XG4gICAgICAgICAgICAgICAgZmlsZU5hbWU6IF9qc3hGaWxlTmFtZSxcbiAgICAgICAgICAgICAgICBsaW5lTnVtYmVyOiA1NVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgJ0JpZGRpbmdzJ1xuICAgICAgICAgICksXG4gICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcbiAgICAgICAgICAgIExpbmssXG4gICAgICAgICAgICB7IHJvdXRlOiAnL2JpZC9uZXcnLCBfX3NvdXJjZToge1xuICAgICAgICAgICAgICAgIGZpbGVOYW1lOiBfanN4RmlsZU5hbWUsXG4gICAgICAgICAgICAgICAgbGluZU51bWJlcjogNTdcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgICAgICdhJyxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIF9fc291cmNlOiB7XG4gICAgICAgICAgICAgICAgICBmaWxlTmFtZTogX2pzeEZpbGVOYW1lLFxuICAgICAgICAgICAgICAgICAgbGluZU51bWJlcjogNThcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoQnV0dG9uLCB7XG4gICAgICAgICAgICAgICAgZmxvYXRlZDogJ3JpZ2h0JyxcbiAgICAgICAgICAgICAgICBjb250ZW50OiAnQ3JlYXRlIG5ldyBCaWRkaW5nJyxcbiAgICAgICAgICAgICAgICBpY29uOiAnYWRkIGNpcmNsZScsXG4gICAgICAgICAgICAgICAgcHJpbWFyeTogdHJ1ZSxcbiAgICAgICAgICAgICAgICBfX3NvdXJjZToge1xuICAgICAgICAgICAgICAgICAgZmlsZU5hbWU6IF9qc3hGaWxlTmFtZSxcbiAgICAgICAgICAgICAgICAgIGxpbmVOdW1iZXI6IDU5XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgKVxuICAgICAgICAgICksXG4gICAgICAgICAgdGhpcy5yZW5kZXJCaWRkaW5ncygpXG4gICAgICAgIClcbiAgICAgICk7XG4gICAgfVxuICB9XSwgW3tcbiAgICBrZXk6ICdnZXRJbml0aWFsUHJvcHMnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiAoKSB7XG4gICAgICB2YXIgX3JlZiA9IF9hc3luY1RvR2VuZXJhdG9yKCAvKiNfX1BVUkVfXyovX3JlZ2VuZXJhdG9yUnVudGltZS5tYXJrKGZ1bmN0aW9uIF9jYWxsZWUyKCkge1xuICAgICAgICB2YXIgX3RoaXMzID0gdGhpcztcblxuICAgICAgICB2YXIgYmlkZGluZ3MsIGl0ZW1zO1xuICAgICAgICByZXR1cm4gX3JlZ2VuZXJhdG9yUnVudGltZS53cmFwKGZ1bmN0aW9uIF9jYWxsZWUyJChfY29udGV4dDIpIHtcbiAgICAgICAgICB3aGlsZSAoMSkge1xuICAgICAgICAgICAgc3dpdGNoIChfY29udGV4dDIucHJldiA9IF9jb250ZXh0Mi5uZXh0KSB7XG4gICAgICAgICAgICAgIGNhc2UgMDpcbiAgICAgICAgICAgICAgICBfY29udGV4dDIubmV4dCA9IDI7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGJpZGRpbmdDcmVhdG9yLm1ldGhvZHMuZ2V0QmlkZGluZ3MoKS5jYWxsKCk7XG5cbiAgICAgICAgICAgICAgY2FzZSAyOlxuICAgICAgICAgICAgICAgIGJpZGRpbmdzID0gX2NvbnRleHQyLnNlbnQ7XG4gICAgICAgICAgICAgICAgX2NvbnRleHQyLm5leHQgPSA1O1xuICAgICAgICAgICAgICAgIHJldHVybiBfUHJvbWlzZS5hbGwoYmlkZGluZ3MubWFwKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgIHZhciBfcmVmMiA9IF9hc3luY1RvR2VuZXJhdG9yKCAvKiNfX1BVUkVfXyovX3JlZ2VuZXJhdG9yUnVudGltZS5tYXJrKGZ1bmN0aW9uIF9jYWxsZWUoYWRkcmVzcykge1xuICAgICAgICAgICAgICAgICAgICB2YXIgc3VtbWFyeTtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIF9yZWdlbmVyYXRvclJ1bnRpbWUud3JhcChmdW5jdGlvbiBfY2FsbGVlJChfY29udGV4dCkge1xuICAgICAgICAgICAgICAgICAgICAgIHdoaWxlICgxKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzd2l0Y2ggKF9jb250ZXh0LnByZXYgPSBfY29udGV4dC5uZXh0KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIGNhc2UgMDpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfY29udGV4dC5uZXh0ID0gMjtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gYmlkZGluZ0NyZWF0b3IubWV0aG9kcy52aWV3QmlkZGluZyhhZGRyZXNzKS5jYWxsKCk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgY2FzZSAyOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN1bW1hcnkgPSBfY29udGV4dC5zZW50O1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBfY29udGV4dC5hYnJ1cHQoJ3JldHVybicsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhlYWRlcjogc3VtbWFyeVswXSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBcIkN1cnJlbnQgSGlnaGVzdCBCaWQ6IFwiICsgd2ViMy51dGlscy5mcm9tV2VpKHN1bW1hcnlbMV0sICdldGhlcicpICsgXCIgRVRIXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtZXRhOiBhZGRyZXNzXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgY2FzZSA0OlxuICAgICAgICAgICAgICAgICAgICAgICAgICBjYXNlICdlbmQnOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBfY29udGV4dC5zdG9wKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9LCBfY2FsbGVlLCBfdGhpczMpO1xuICAgICAgICAgICAgICAgICAgfSkpO1xuXG4gICAgICAgICAgICAgICAgICByZXR1cm4gZnVuY3Rpb24gKF94KSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBfcmVmMi5hcHBseSh0aGlzLCBhcmd1bWVudHMpO1xuICAgICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgICB9KCkpKTtcblxuICAgICAgICAgICAgICBjYXNlIDU6XG4gICAgICAgICAgICAgICAgaXRlbXMgPSBfY29udGV4dDIuc2VudDtcbiAgICAgICAgICAgICAgICByZXR1cm4gX2NvbnRleHQyLmFicnVwdCgncmV0dXJuJywgeyBiaWRkaW5nczogYmlkZGluZ3MsIGl0ZW1zOiBpdGVtcyB9KTtcblxuICAgICAgICAgICAgICBjYXNlIDc6XG4gICAgICAgICAgICAgIGNhc2UgJ2VuZCc6XG4gICAgICAgICAgICAgICAgcmV0dXJuIF9jb250ZXh0Mi5zdG9wKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9LCBfY2FsbGVlMiwgdGhpcyk7XG4gICAgICB9KSk7XG5cbiAgICAgIGZ1bmN0aW9uIGdldEluaXRpYWxQcm9wcygpIHtcbiAgICAgICAgcmV0dXJuIF9yZWYuYXBwbHkodGhpcywgYXJndW1lbnRzKTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIGdldEluaXRpYWxQcm9wcztcbiAgICB9KClcbiAgfV0pO1xuXG4gIHJldHVybiBCaWRkaW5nQ3JlYXRvcjtcbn0oQ29tcG9uZW50KTtcblxuZXhwb3J0IGRlZmF1bHQgQmlkZGluZ0NyZWF0b3I7Il19
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInVua25vd24iXSwibmFtZXMiOlsiX2pzeEZpbGVOYW1lIiwiQmlkZGluZ0NyZWF0b3IiLCJfQ29tcG9uZW50IiwiX19wcm90b19fIiwiYXBwbHkiLCJhcmd1bWVudHMiLCJrZXkiLCJ2YWx1ZSIsInJlbmRlckJpZGRpbmdzIiwiX3RoaXMyIiwiaXRlbXMiLCJwcm9wcyIsImJpZGRpbmdzIiwibWFwIiwiYWRkcmVzcyIsImkiLCJjb25zb2xlIiwibG9nIiwiaGVhZGVyIiwiZGVzY3JpcHRpb24iLCJtZXRhIiwiY3JlYXRlRWxlbWVudCIsInJvdXRlIiwiX19zb3VyY2UiLCJmaWxlTmFtZSIsImxpbmVOdW1iZXIiLCJmbHVpZCIsIkdyb3VwIiwicmVuZGVyIiwiZmxvYXRlZCIsImNvbnRlbnQiLCJpY29uIiwicHJpbWFyeSIsIl9yZWYiLCJtYXJrIiwiX2NhbGxlZTIiLCJfdGhpczMiLCJ3cmFwIiwiX2NhbGxlZTIkIiwiX2NvbnRleHQyIiwicHJldiIsIm5leHQiLCJtZXRob2RzIiwiZ2V0QmlkZGluZ3MiLCJjYWxsIiwic2VudCIsImFsbCIsIl9yZWYyIiwiX2NhbGxlZSIsInN1bW1hcnkiLCJfY2FsbGVlJCIsIl9jb250ZXh0Iiwidmlld0JpZGRpbmciLCJhYnJ1cHQiLCJ1dGlscyIsImZyb21XZWkiLCJzdG9wIiwiX3giLCJnZXRJbml0aWFsUHJvcHMiXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFFQTs7OztBQUNBOzs7O0FBQ0E7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBTkEsSUFBSUEsZUFBZSxxRUFBbkI7OztBQVFBLElBQUlDLGlCQUFpQixVQUFVQyxVQUFWLEVBQXNCO0FBQ3pDLDBCQUFVRCxjQUFWLEVBQTBCQyxVQUExQjs7QUFFQSxXQUFTRCxjQUFULEdBQTBCO0FBQ3hCLGtDQUFnQixJQUFoQixFQUFzQkEsY0FBdEI7O0FBRUEsV0FBTyx5Q0FBMkIsSUFBM0IsRUFBaUMsQ0FBQ0EsZUFBZUUsU0FBZixJQUE0Qiw4QkFBdUJGLGNBQXZCLENBQTdCLEVBQXFFRyxLQUFyRSxDQUEyRSxJQUEzRSxFQUFpRkMsU0FBakYsQ0FBakMsQ0FBUDtBQUNEOztBQUVELDZCQUFhSixjQUFiLEVBQTZCLENBQUM7QUFDNUJLLFNBQUssZ0JBRHVCO0FBRTVCQyxXQUFPLFNBQVNDLGNBQVQsR0FBMEI7QUFDL0IsVUFBSUMsU0FBUyxJQUFiOztBQUVBLFVBQUlDLFFBQVEsS0FBS0MsS0FBTCxDQUFXQyxRQUFYLENBQW9CQyxHQUFwQixDQUF3QixVQUFVQyxPQUFWLEVBQW1CQyxDQUFuQixFQUFzQjtBQUN4REMsZ0JBQVFDLEdBQVIsQ0FBWUYsQ0FBWjtBQUNBQyxnQkFBUUMsR0FBUixDQUFZSCxPQUFaO0FBQ0EsZUFBTztBQUNMSSxrQkFBUVQsT0FBT0UsS0FBUCxDQUFhRCxLQUFiLENBQW1CSyxDQUFuQixFQUFzQkcsTUFEekI7QUFFTEMsdUJBQWFWLE9BQU9FLEtBQVAsQ0FBYUQsS0FBYixDQUFtQkssQ0FBbkIsRUFBc0JJLFdBRjlCO0FBR0xDLGdCQUFNLGdCQUFNQyxhQUFOLGVBRUosRUFBRUMsT0FBTyxVQUFVUixPQUFuQixFQUE0QlMsVUFBVTtBQUNsQ0Msd0JBQVV4QixZQUR3QjtBQUVsQ3lCLDBCQUFZO0FBRnNCO0FBQXRDLFdBRkksRUFPSixnQkFBTUosYUFBTixDQUNFLEdBREYsRUFFRTtBQUNFRSxzQkFBVTtBQUNSQyx3QkFBVXhCLFlBREY7QUFFUnlCLDBCQUFZO0FBRko7QUFEWixXQUZGLEVBUUVoQixPQUFPRSxLQUFQLENBQWFELEtBQWIsQ0FBbUJLLENBQW5CLEVBQXNCSyxJQVJ4QixDQVBJLENBSEQ7QUFxQkxNLGlCQUFPO0FBckJGLFNBQVA7QUF1QkQsT0ExQlcsQ0FBWjs7QUE0QkEsYUFBTyxnQkFBTUwsYUFBTixDQUFvQixzQkFBS00sS0FBekIsRUFBZ0MsRUFBRWpCLE9BQU9BLEtBQVQsRUFBZ0JhLFVBQVU7QUFDN0RDLG9CQUFVeEIsWUFEbUQ7QUFFN0R5QixzQkFBWTtBQUZpRDtBQUExQixPQUFoQyxDQUFQO0FBS0Q7QUF0QzJCLEdBQUQsRUF1QzFCO0FBQ0RuQixTQUFLLFFBREo7QUFFREMsV0FBTyxTQUFTcUIsTUFBVCxHQUFrQjs7QUFFdkIsYUFBTyxnQkFBTVAsYUFBTixtQkFFTDtBQUNFRSxrQkFBVTtBQUNSQyxvQkFBVXhCLFlBREY7QUFFUnlCLHNCQUFZO0FBRko7QUFEWixPQUZLLEVBUUwsZ0JBQU1KLGFBQU4sQ0FDRSxLQURGLEVBRUU7QUFDRUUsa0JBQVU7QUFDUkMsb0JBQVV4QixZQURGO0FBRVJ5QixzQkFBWTtBQUZKO0FBRFosT0FGRixFQVFFLGdCQUFNSixhQUFOLENBQ0UsSUFERixFQUVFO0FBQ0VFLGtCQUFVO0FBQ1JDLG9CQUFVeEIsWUFERjtBQUVSeUIsc0JBQVk7QUFGSjtBQURaLE9BRkYsRUFRRSxVQVJGLENBUkYsRUFrQkUsZ0JBQU1KLGFBQU4sZUFFRSxFQUFFQyxPQUFPLFVBQVQsRUFBcUJDLFVBQVU7QUFDM0JDLG9CQUFVeEIsWUFEaUI7QUFFM0J5QixzQkFBWTtBQUZlO0FBQS9CLE9BRkYsRUFPRSxnQkFBTUosYUFBTixDQUNFLEdBREYsRUFFRTtBQUNFRSxrQkFBVTtBQUNSQyxvQkFBVXhCLFlBREY7QUFFUnlCLHNCQUFZO0FBRko7QUFEWixPQUZGLEVBUUUsZ0JBQU1KLGFBQU4sMEJBQTRCO0FBQzFCUSxpQkFBUyxPQURpQjtBQUUxQkMsaUJBQVMsb0JBRmlCO0FBRzFCQyxjQUFNLFlBSG9CO0FBSTFCQyxpQkFBUyxJQUppQjtBQUsxQlQsa0JBQVU7QUFDUkMsb0JBQVV4QixZQURGO0FBRVJ5QixzQkFBWTtBQUZKO0FBTGdCLE9BQTVCLENBUkYsQ0FQRixDQWxCRixFQTZDRSxLQUFLakIsY0FBTCxFQTdDRixDQVJLLENBQVA7QUF3REQ7QUE1REEsR0F2QzBCLENBQTdCLEVBb0dJLENBQUM7QUFDSEYsU0FBSyxpQkFERjtBQUVIQyxXQUFPLFlBQVk7QUFDakIsVUFBSTBCLE9BQU8saUNBQW1CLGFBQWEsc0JBQW9CQyxJQUFwQixDQUF5QixTQUFTQyxRQUFULEdBQW9CO0FBQ3RGLFlBQUlDLFNBQVMsSUFBYjs7QUFFQSxZQUFJeEIsUUFBSixFQUFjRixLQUFkO0FBQ0EsZUFBTyxzQkFBb0IyQixJQUFwQixDQUF5QixTQUFTQyxTQUFULENBQW1CQyxTQUFuQixFQUE4QjtBQUM1RCxpQkFBTyxDQUFQLEVBQVU7QUFDUixvQkFBUUEsVUFBVUMsSUFBVixHQUFpQkQsVUFBVUUsSUFBbkM7QUFDRSxtQkFBSyxDQUFMO0FBQ0VGLDBCQUFVRSxJQUFWLEdBQWlCLENBQWpCO0FBQ0EsdUJBQU8seUJBQWVDLE9BQWYsQ0FBdUJDLFdBQXZCLEdBQXFDQyxJQUFyQyxFQUFQOztBQUVGLG1CQUFLLENBQUw7QUFDRWhDLDJCQUFXMkIsVUFBVU0sSUFBckI7QUFDQU4sMEJBQVVFLElBQVYsR0FBaUIsQ0FBakI7QUFDQSx1QkFBTyxrQkFBU0ssR0FBVCxDQUFhbEMsU0FBU0MsR0FBVCxDQUFhLFlBQVk7QUFDM0Msc0JBQUlrQyxRQUFRLGlDQUFtQixhQUFhLHNCQUFvQmIsSUFBcEIsQ0FBeUIsU0FBU2MsT0FBVCxDQUFpQmxDLE9BQWpCLEVBQTBCO0FBQzdGLHdCQUFJbUMsT0FBSjtBQUNBLDJCQUFPLHNCQUFvQlosSUFBcEIsQ0FBeUIsU0FBU2EsUUFBVCxDQUFrQkMsUUFBbEIsRUFBNEI7QUFDMUQsNkJBQU8sQ0FBUCxFQUFVO0FBQ1IsZ0NBQVFBLFNBQVNYLElBQVQsR0FBZ0JXLFNBQVNWLElBQWpDO0FBQ0UsK0JBQUssQ0FBTDtBQUNFVSxxQ0FBU1YsSUFBVCxHQUFnQixDQUFoQjtBQUNBLG1DQUFPLHlCQUFlQyxPQUFmLENBQXVCVSxXQUF2QixDQUFtQ3RDLE9BQW5DLEVBQTRDOEIsSUFBNUMsRUFBUDs7QUFFRiwrQkFBSyxDQUFMO0FBQ0VLLHNDQUFVRSxTQUFTTixJQUFuQjtBQUNBLG1DQUFPTSxTQUFTRSxNQUFULENBQWdCLFFBQWhCLEVBQTBCO0FBQy9CbkMsc0NBQVErQixRQUFRLENBQVIsQ0FEdUI7QUFFL0I5QiwyQ0FBYSwwQkFBMEIsY0FBS21DLEtBQUwsQ0FBV0MsT0FBWCxDQUFtQk4sUUFBUSxDQUFSLENBQW5CLEVBQStCLE9BQS9CLENBQTFCLEdBQW9FLE1BRmxEO0FBRy9CN0Isb0NBQU1OO0FBSHlCLDZCQUExQixDQUFQOztBQU1GLCtCQUFLLENBQUw7QUFDQSwrQkFBSyxLQUFMO0FBQ0UsbUNBQU9xQyxTQUFTSyxJQUFULEVBQVA7QUFmSjtBQWlCRDtBQUNGLHFCQXBCTSxFQW9CSlIsT0FwQkksRUFvQktaLE1BcEJMLENBQVA7QUFxQkQsbUJBdkIyQyxDQUFoQyxDQUFaOztBQXlCQSx5QkFBTyxVQUFVcUIsRUFBVixFQUFjO0FBQ25CLDJCQUFPVixNQUFNM0MsS0FBTixDQUFZLElBQVosRUFBa0JDLFNBQWxCLENBQVA7QUFDRCxtQkFGRDtBQUdELGlCQTdCZ0MsRUFBYixDQUFiLENBQVA7O0FBK0JGLG1CQUFLLENBQUw7QUFDRUssd0JBQVE2QixVQUFVTSxJQUFsQjtBQUNBLHVCQUFPTixVQUFVYyxNQUFWLENBQWlCLFFBQWpCLEVBQTJCLEVBQUV6QyxVQUFVQSxRQUFaLEVBQXNCRixPQUFPQSxLQUE3QixFQUEzQixDQUFQOztBQUVGLG1CQUFLLENBQUw7QUFDQSxtQkFBSyxLQUFMO0FBQ0UsdUJBQU82QixVQUFVaUIsSUFBVixFQUFQO0FBN0NKO0FBK0NEO0FBQ0YsU0FsRE0sRUFrREpyQixRQWxESSxFQWtETSxJQWxETixDQUFQO0FBbURELE9BdkQwQyxDQUFoQyxDQUFYOztBQXlEQSxlQUFTdUIsZUFBVCxHQUEyQjtBQUN6QixlQUFPekIsS0FBSzdCLEtBQUwsQ0FBVyxJQUFYLEVBQWlCQyxTQUFqQixDQUFQO0FBQ0Q7O0FBRUQsYUFBT3FELGVBQVA7QUFDRCxLQS9ETTtBQUZKLEdBQUQsQ0FwR0o7O0FBd0tBLFNBQU96RCxjQUFQO0FBQ0QsQ0FsTG9CLGtCQUFyQjs7a0JBb0xlQSxjIiwiZmlsZSI6InVua25vd24ifQ==