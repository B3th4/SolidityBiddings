pragma solidity ^0.4.24;

contract User{

    address public owner;
    string public description;

    constructor(string _description, address _owner) public {
        owner = _owner;
        description = _description;

    }


    modifier restricted() {
        require(msg.sender == owner);
        _;
    }

    function changeDescription(string newDescription) public restricted{
        description=newDescription;
    }

    function getDescription() public view returns(string){
        return description;
    }

}
contract Item{
    struct History{
        address owner;
        uint price;
        uint date;
    }

    uint historyLength=0;
    string public name;
    string public description;
    History[] public history;
    address public owner;
    address public masterContract;


    constructor(string _name, string _description, uint _price, uint _time, address _owner, address _mastercontract) public{
        name=_name;
        description=_description;
        owner=_owner;
        changeHistory(_owner,_price,_time);
        masterContract=_mastercontract;

    }

        modifier restricted() {
        require((tx.origin == owner || msg.sender == masterContract), 'you are not an owner of item');
        _;
    }

    function changeDescription(string _description) public restricted{
        description=_description;

    }
    function changeHistory(address newOwner,uint price, uint date) private{
        History memory newHistory = History({
            owner: newOwner,
            price: price,
            date: date
        });

        history.push(newHistory);
        historyLength++;
    }

    function changeOwner(address newOwner, uint price, uint time)public restricted{
        changeHistory(newOwner,price,time);
        owner=newOwner;
    }


    function getHistory(uint index) public view returns(address,uint,uint){
        return (history[index].owner,
                history[index].price,
                history[index].date);

    }


    function summaryItem() public view returns(string, string, address, uint){
        return (name, description, owner, historyLength);
    }

}
contract Bidding{
    address public item;
    address public creator;
    address public highestBidder;
    uint public currentBid;
    uint public minimumBid;
    bool public finished;
    address public masterContract;

    constructor(address _item,address _creator,uint _minimumBid, address _masterContract) public {
        item=_item;
        creator=_creator;
        highestBidder=_creator;
        minimumBid=_minimumBid;
        currentBid= 0;
        finished = false;
        masterContract=_masterContract;

    }

    modifier restricted() {
        require(tx.origin == creator);
        _;
    }

    modifier masterContractCheck(){
      require(msg.sender==masterContract);
      _;
    }

    function bid() public payable{
        uint amount=msg.value;
        address bidder = msg.sender;
        require(amount>currentBid,"You offered less than the current bid price");
        require(!finished, "This bidding has ended");

        if(currentBid!=0){
          highestBidder.transfer(currentBid);
        }

        currentBid=amount;
        highestBidder=bidder;


    }

    function endBidding() public restricted{
        finished =true;
    }


    function giveEth(address _receiver, uint eth) public masterContractCheck{
      _receiver.transfer(eth);
    }

    function summary() public view returns(address, address, address, uint, uint, bool){
        uint value = currentBid;
        if(value == 0){
            value = minimumBid;
        }
        return (item, creator, highestBidder, minimumBid, value, finished);
    }

}

contract BiddingCreator{
    struct UserInfo{
        address user;
        bool userExists;
        address[] items;
        mapping(address => uint) itemsPresent; //default =0, has object =1, in bidding 2, had object =3

    }

    struct BidInfo{
        uint status;
        uint index;
    }

    mapping(address => address) itemInBid;
    mapping(address=>UserInfo) UserInfoCollection;
    mapping(address => address) userContractToUser;
    uint public index = 0;
    address[] public biddings;
    mapping(address => BidInfo) biddingStatus; //default =0 = ongoing =1, stopped =2

    modifier userMustExist() {
        require((UserInfoCollection[msg.sender].userExists), "User does not exist");
        _;
    }

    modifier userMustHaveItem(address _item){
        require((UserInfoCollection[msg.sender].itemsPresent[_item]==1),"user does not posses object");
        _;
    }

    function createUser(string description) public {
        require(!UserInfoCollection[msg.sender].userExists, "already an user for this address");
        address newUser = new User(description, msg.sender);
        UserInfo memory userinfo;
        userinfo.user=newUser;
        userinfo.userExists=true;
        userContractToUser[newUser]=msg.sender;
        UserInfoCollection[msg.sender]=userinfo;
    }

    function createItem(string name, string description, uint value,uint time) public userMustExist() {
        address newItem = new Item(name,description,value,time,msg.sender,address(this));
        UserInfoCollection[msg.sender].items.push(newItem);
        UserInfoCollection[msg.sender].itemsPresent[newItem]= 1;
    }

    function giveItem(address _item, address _receiver, uint price, uint time) public userMustExist userMustHaveItem(_item){
        //TODO need to test if receiver exists to!
        Item item =Item(_item);
        address owner = item.owner();

        item.changeOwner(_receiver,price,time);

        if(owner != address(this)){
          UserInfoCollection[owner].itemsPresent[_item]=2;

          if(UserInfoCollection[userContractToUser[_receiver]].itemsPresent[_item]==2 || UserInfoCollection[userContractToUser[_receiver]].itemsPresent[_item]==3){
              UserInfoCollection[userContractToUser[_receiver]].itemsPresent[_item]=1;
          }
          else{
              UserInfoCollection[userContractToUser[_receiver]].items.push(_item);
              UserInfoCollection[userContractToUser[_receiver]].itemsPresent[_item]=1;
          }
        }
    }

    function giveItemBack(address _item, address _newOwner, uint _price, uint _time)private{
        Item item = Item(_item);
        item.changeOwner(_newOwner,_price,_time);

        if(UserInfoCollection[_newOwner].itemsPresent[_item]==3){
            UserInfoCollection[_newOwner].itemsPresent[_item]=1;
        }
        else{
            UserInfoCollection[_newOwner].items.push(_item);
            UserInfoCollection[_newOwner].itemsPresent[_item]=1;
        }
    }
/**
    function giveEth(address _bidding,address _receiver, uint eth) public {
      Bidding toFinalize = Bidding(_bidding);
      toFinalize.giveEth(_receiver,eth);
    }
*/
    function createBidding(address _item, address _owner, uint _minimumBid, uint _time) public userMustExist userMustHaveItem(_item) {
        require(isThereBiddingForItem(_item)==false);
        address newBidding = new Bidding(_item,_owner,_minimumBid,address(this));
        giveItem(_item,address(this),0,_time);
        biddings.push(newBidding);
        biddingStatus[newBidding].status=1;
        biddingStatus[newBidding].index=index;
        itemInBid[_item]=newBidding;
        index++;

    }




    function finalizeBidding(address _bidding, uint _time) public{
        Bidding toFinalize = Bidding(_bidding);
        address owner = toFinalize.creator();
        require(tx.origin == owner);
        require(biddingStatus[toFinalize].status==1);

        toFinalize.endBidding();
        UserInfoCollection[owner].itemsPresent[toFinalize.item()]=3;
        if(toFinalize.currentBid()>toFinalize.minimumBid()){
          giveItemBack(toFinalize.item(),toFinalize.highestBidder(),toFinalize.currentBid(),_time);
          toFinalize.giveEth(owner,toFinalize.currentBid());
        }
        else{
            giveItem(toFinalize.item(),toFinalize.highestBidder(),toFinalize.currentBid(),_time);
        }

        biddingStatus[toFinalize].status=2;

    }


    function userExists(address _user) public view returns(bool){
        return UserInfoCollection[_user].userExists;
    }

    function getUser(address _user) public view returns (address) {
        return UserInfoCollection[_user].user;
    }

    function getBiddings() public view returns (address []){
      return biddings;
    }

    function viewBidding(address biddingAddress) public view returns (string, uint,bool){
        Bidding bidding =Bidding(biddingAddress);
        address itemAddress = bidding.item();
        Item item= Item(itemAddress);
        string memory name = item.name();
        uint amount = bidding.currentBid();
        if (amount == 0){
            amount = bidding.minimumBid();
        }
        bool status=bidding.finished();


        return(name,amount,status);

    }

    function getItemsFromUser(address _user) public view returns (address[]){
        return UserInfoCollection[_user].items;
    }

    function doesUserHasItem(address _user, address _item)public view returns(uint){
        return UserInfoCollection[_user].itemsPresent[_item];
    }

    function isThereBiddingForItem(address _item) public view returns (bool){
        return (biddingStatus[itemInBid[_item]].status==1);
    }
    function getBiddingForItem(address _item) public view returns (address){
        require(isThereBiddingForItem(_item));
        return itemInBid[_item];
    }
    function getInfoUser(address _user) public view returns (address, bool, address[]){
      return(
        UserInfoCollection[_user].user,
        UserInfoCollection[_user].userExists,
        UserInfoCollection[_user].items
        );
    }

    function getOwner(address _item) public view returns (address){
      Item item =Item(_item);
      return item.owner();
    }

    function getKeyFromContract(address _contract) public view returns (address){
        return userContractToUser[_contract];
    }

    function getBiddingStatus(uint _index) public view returns (uint){
        address _item = biddings[_index];
      return biddingStatus[_item].status;
    }
}
