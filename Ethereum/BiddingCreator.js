import web3 from './web3';
import BiddingCreator from './build/BiddingCreator.json'

const instance  = new web3.eth.Contract(
  JSON.parse(BiddingCreator.interface),
  '0x6776aC71e92698639B346CF05603f13e15537D24'   //<-Change this to use another mastercontract
);

export default instance;
