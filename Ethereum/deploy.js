const HDWalletProvider = require('truffle-hdwallet-provider');
const Web3 = require('web3');

const compiledBiddingCreator = require('./build/BiddingCreator.json');

const provider = new HDWalletProvider(
  'home sort entire robust unlock trust net arrest screen window hold mistake',
  'https://rinkeby.infura.io/v3/c14b0d4ca0734921afeef31df8593e70'
);
const web3 = new Web3(provider);

const deploy = async () => {
  const accounts = await web3.eth.getAccounts();

  console.log('Attempting to deploy from account', accounts[0]);

  const result = await new web3.eth.Contract(JSON.parse(compiledBiddingCreator.interface))
    .deploy({ data: '0x' +compiledBiddingCreator.bytecode })
    .send({ gas: '5000000', from: accounts[0] });

  console.log('Contract deployed to', result.options.address);
};
deploy();



//0x26B3aF4Fd791FCD5DA1111a0123365DB81664a39
