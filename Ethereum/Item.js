import web3 from './web3';
import item from './build/Item.json';

export default address => {
  return new web3.eth.Contract(JSON.parse(item.interface), address);
};
