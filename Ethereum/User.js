import web3 from './web3';
import user from './build/User.json';

export default address => {
  return new web3.eth.Contract(JSON.parse(user.interface), address);
};
