import web3 from './web3';
import bid from './build/Bidding.json';

export default address => {
  return new web3.eth.Contract(JSON.parse(bid.interface), address);
};
