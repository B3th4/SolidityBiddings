import React, { Component } from 'react';
import { Form, Input, Message, Button } from 'semantic-ui-react';
import Item from '../Ethereum/Item';
import web3 from '../Ethereum/web3';
import { Router } from '../routes';

class DescriptionItem extends Component {
  state = {
    description: '',
    errorMessage: '',
    loading: false
  };

  onSubmit = async event => {
    event.preventDefault();

    const item = Item(this.props.address);

    this.setState({ loading: true, errorMessage: '' });

    try {
      console.log(this.state.description)
      const accounts = await web3.eth.getAccounts();
      await item.methods.changeDescription(this.state.description).send({
        from: accounts[0],
      });
      Router.replaceRoute(`/item/${this.props.address}`);
    } catch (err) {
      this.setState({ errorMessage: err.message });
    }

    this.setState({ loading: false, value: '' });
  };


  render() {
    return (
      <Form onSubmit={this.onSubmit} error={!!this.state.errorMessage}>
        <Form.Field>
          <label>Change Item description:</label>
          <Input
          value={this.state.description}
          onChange={event => this.setState({description:event.target.value})}
          />
        </Form.Field>

        <Message error header="Oops!" content={this.state.errorMessage} />
        <Button primary loading={this.state.loading}>
          Confirm new Description
        </Button>
      </Form>
    );
  }
}

export default DescriptionItem;
