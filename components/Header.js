import React from 'react';
import { Menu, Button, Icon, activeItem } from 'semantic-ui-react';
import { Link,Router } from '../routes';


export default () => {
  return (
    <Menu style={{ marginTop: '10px' }}>
      <Link route="/">
        <a className="item">Ethereum Bidding</a>
      </Link>

      <Menu.Menu position="right">
        <Link route="/myprofile">
          <a className="item">My Profile</a>
        </Link>

        <Link route="/myitems">
          <a className="item">My Items</a>
        </Link>

        <Link route="/mybiddings">
          <a className="item">My Biddings</a>
        </Link>

        <Link route="/help">
        <Menu.Item
          name='Help'
          active={activeItem === 'Help'}
        >
          <Icon name='question circle outline' />
        </Menu.Item>
        </Link>


      </Menu.Menu>
    </Menu>
  );
};
